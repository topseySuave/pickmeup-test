import {
    StatusBar,
    Dimensions,
} from 'react-native';
import styled from 'styled-components';

export const Container = styled.View`
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: center;
    background-color: #FFF;
    max-height: ${Dimensions.get('window').height}px;
`;

export const GradientBackground = styled.View`
    width: ${Dimensions.get('window').width}px;
    background-color: #024FDB;
    margin-top: -${StatusBar.currentHeight}px;
    display: flex;
    flex: 1;
    justify-content: space-between;
    align-items: center;
    border-bottom-left-radius: 50px;
    border-bottom-right-radius: 50px;
    overflow: hidden;
    padding-bottom: 20px;
`;

export const KeyAvoidingView = styled.KeyboardAvoidingView`
    margin: 50px;
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    border-color: #CCC;
    border-width: 1px;
    border-radius: 10px;
`;

export const CustomTextInput = styled.TextInput`
    height: 40px;
    width: 50%;
    border-left-color: #CCC;
    border-left-width: 1px;
    padding: 0 10px;
`;

export const InputInfo = styled.Text`
    color: #CCC;
    font-size: 12px;
    padding: 0 50px;
`;

export const SelectField = styled.Picker`
    height: 50px;
    width: 130px;
`;

export const Seperator = styled.View`
    width: 100px;
    height: 1px;
    margin: 0 20px;
    background-color: #CCC;
`;

export const ImageLogo = styled.Image`
    margin-top: 80px;
    resize-mode: stretch;
`;

export const DividerText = styled.Text`
    color: #FFF;
`;

export const InfoView = styled.View`
    margin-top: 0px;
`;

export const HeaderSectionMainText = styled.Text`
    font-size: 30px;
    font-weight: 700;
    color: #FFF;
`;

export const HeaderSectionSubText = styled.Text`
    font-size: 20px;
    font-weight: 500;
    color: #FFF;
`;

export const SocialButtons = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    width: 80%;
`;
