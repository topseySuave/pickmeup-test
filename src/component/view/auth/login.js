import React, { useState } from 'react';
import {
  Alert,
  View,
  StatusBar,
} from 'react-native';
import {
  Container,
  GradientBackground,
  KeyAvoidingView,
  InputInfo,
  CustomTextInput,
  SelectField,
  Seperator,
  DividerText,
  InfoView,
  ImageLogo,
  HeaderSectionMainText,
  HeaderSectionSubText,
  SocialButtons,
} from './styles';
import SocialButton from '../../widget/buttons';

const login = () => {
  // Define the phone number text field input state
  const [text, setText] = useState('');

  // define the country value for state change,
  // when the user selects a country
  const [country, setCountry] = useState('🇺🇸 +1');

  // Initialize select list items
  const items = [
    { key: 'USA', label: '🇺🇸 +1' },
    { key: 'NGN', label: '🇳🇬 +234' },
    { key: 'GHN', label: '🇬🇭 +233' },
  ];

  return (
    <>
      <StatusBar barStyle="light-content" />
      <Container>
        <GradientBackground>
          <ImageLogo
            source={require('../../../assets/pickmeup.jpeg')}
          />
          <InfoView>
            <HeaderSectionMainText>Hey there!</HeaderSectionMainText>
            <HeaderSectionSubText>
              Sign in with Social Media
            </HeaderSectionSubText>
            <SocialButtons>
              <SocialButton buttonType="facebook" />
              <SocialButton buttonType="google" />
            </SocialButtons>
          </InfoView>
          <Seperator />
          <DividerText>
            Or type your phone number to get started
          </DividerText>
        </GradientBackground>
        <View>
          <KeyAvoidingView>
            <SelectField
              selectedValue={country}
              onValueChange={(itemValue, itemIndex) => setCountry(itemValue)}
            >
              {
                items.map(item =>
                  <SelectField.Item
                    key={item.key}
                    label={item.label}
                    value={item.label}
                  />)
              }
            </SelectField>
            <CustomTextInput
              placeholder="Enter phone number!"
              onChangeText={textInput => setText(textInput)}
              defaultValue={text}
            />
          </KeyAvoidingView>
          <InputInfo>
            A 4 digits OTP will be sent via SMS to verify your mobile number!
          </InputInfo>
        </View>
      </Container>
    </>
  );
};

export default login;
