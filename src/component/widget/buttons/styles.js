import styled from 'styled-components';

export const SocialButton = styled.TouchableOpacity`
    height: 40px;
    width: 150px;
    border-radius: 10px;
    background-color: white;
    margin-top: 20px;
    justify-content: center;
    align-items: center;
`;

export const FbText = styled.Text`
color: #024FDB;
`;

export const GoogleText = styled.Text`
color: #DE5246;
`;