import React from 'react';
import {Alert} from 'react-native';

import {SocialButton, FbText, GoogleText} from './styles';

// define button type and it should ascept facebook or google as props
// can be modified in the future.
const Button = ({ buttonType = 'facebook' || 'google' }) => (
    <SocialButton
        onPress={() => Alert.alert(`login with ${buttonType}`)}
    >
        {buttonType === 'facebook' && <FbText>Facebook</FbText>}
        {buttonType === 'google' && <GoogleText>Google</GoogleText>}
    </SocialButton>
);

export default Button;
